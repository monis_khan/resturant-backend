//@ define express
var express = require('express');

//@ desine mongoose
var mongoose = require('mongoose');

var app = express() ;

//@ define port
var port = process.env.PORT || 5000;

app.listen(port);