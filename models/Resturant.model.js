var mongoose = require('mongoose');
var Schema = mongoose.Schema ;

var resturantSchema = new Schema({
	name : {
		type:String
	},
	address : {
		streetAddres1:{
			type:String
		},
		streetAddres2:{
			type:String
		},
		pinCode:{
			type:Number
		},
		state:{
			type:String
		}
		country:{
			type:String
		}
	},
	timings:{
		from :{
			type:String
		},
		to:{
			type:String
		}
	},
	tags:{
		type:Array
	},
	pictures:{
		type:Array
	},
	stars:{
		type:Array
	}
})